class_name Player

#extends KinematicBody2D

extends SGKinematicBody2D

enum States {Idle = 1, Run, Jump, Double_Jump, Fall, Neutral_Attack, Neutral_Attack_2, Run_Attack, Neutral_Attack_3, Charge, Charge_Attack, Up_Charge, Up_Charge_Attack, N_Air, Up_Air, Air_Kick, D_Air, D_Air_Land, Hurt_Check, Small_Hit, Big_Hit, Vert_Hit, Ledge_Grab, Ledge_Climb, Ledge_Roll, Shield, Shield_Roll, Shield_Broken, Down}
var machine_state = States.Idle

const SPEED = 196608
const GRAVITY = 32768 / 2
const JUMP = 65536*8
const DOUBLE_JUMP = 65536*7
const IMPULSE = 65536*2

#var velocity := SGFixedVector2.new()
var velocity := SGFixed.from_float_vector2(Vector2.ZERO)
var up_direction := SGFixed.vector2(0, -65536)

var zero_velocity := SGFixed.vector2(0,0)

var impulse := SGFixed.vector2(IMPULSE, -IMPULSE)


const UP_DIRECTION := Vector2.UP

export var speed := 400

var backwards 
var roll_dir

var shield_disabled

var stock : int = 3

var hit_size : String = ""

var attack_combo = 1
var comboing : bool

export var jump_strength := 700
export var maxiumum_jumps := 2
export var double_jump_strength := 500
export var gravity := GRAVITY
export var maxiumum_fall_speed := 1000
export(int) var friction: int = 80
export(int) var acceleration: int = 100
export var d_Air_acceleration: int = 75
export(int) var _horizontal_direction


var impulse_x 
var impulse_y 
var damage_dealt : int
var player_dir : int = 1
var damage_multiplier 
var opponent_multiplier
var opponent_dir
var health : int

var recieve_impulse_x 
var recieve_impulse_y 
var recieve_damage_dealt: int
var recieve_damage_mul 



export var max_shield_health : float = 300
export var shield_health : float = 0

var shield_glow : float = 0.00



var velocity_near_ground = 0

onready var tween = $Tween

var anim_state_machine 


var can_control :bool


var _jumps_made := 0
var _up_airs := 0
var _air_kicks := 0
#export var _velocity := Vector2.ZERO


var look_at = 1

var vert_raycast_origin 
var vert_raycast_collision_point 
var vert_raycast_distance 

var collision_point := SGFixed.vector2(0,0)
var collision_point_var 

var ledge_roll_distance
var ledge_climb_distance

var shield_roll_distance

var correct_vert_distance 
var correct_horz_distance 

onready var player_collision = 	$SGCollisionShape2D

onready var bottom_raycast = $bottom_raycast
onready var top_raycast = $top_raycast
onready var vertical_raycast = $vertical_ray_Cast
onready var ground_raycast = $ground_raycast
onready var wall_check = $wall_check
onready var wall_check_2 = $wall_check2

onready var hurtbox = $HurtBox
onready var hurtbox_collision = $HurtBox/Hurtbox_Collision
onready var shield_collision = $Shield/Shield_Collision

#onready var vert_hitbox = $Hitbox_Vertical
#onready var vertical_hitbox_collision = $Hitbox_Vertical/Hitbox_Vertical_Collision
onready var hitbox = $Sword_Hitbox
onready var hitbox_collision = $Sword_Hitbox/Sword_Hitbox_Collision
onready var up_hitbox_collision = $Sword_Hitbox_up/Sword_Hitbox_up_Collision
onready var d_air_splash = $D_Air_Splash_Damage
onready var d_air_splash_collision = $D_Air_Splash_Damage/D_Air_splash_collision

onready var anim_player = $NetworkAnimationPlayer

#onready var animPlayerInvincibility = $AnimationPlayerInvincible

onready var networkTimer = $NetworkTimer

var horz_raycast_origin 
var horz_raycast_collision_point 
var horz_raycast_distance 

onready var time_difference
onready var damage_mul


var move_position 
var player_position 
var first_move_position 
var second_move_position 


var colliders
var _hitbox_parent = self

var player_name : String = ""

func _ready():

	machine_state = States.Idle
	shield_health = 300
	_jumps_made = 0
	_air_kicks = 0
	_up_airs = 0

	EventBus.connect("shieldRegen", self, "_on_Shield_regen")
	EventBus.connect("invincible", self, "_invincible")
	EventBus.connect("hitFreeze", self, "_freezeFunction")
	EventBus.connect("hurtBoxShieldCollision", self, "_hurtbox_and_shield_collision")
	EventBus.emit_signal("healthUpdate", health, "player_1")
	EventBus.connect("verticalHitbox", self, "_vertical_Hitbox_Collision")
	EventBus.connect("smoke", self, "_smokeEnabled")
	EventBus.connect("hurtboxhit", self, "_on_HurtBox_hit")
	EventBus.connect("shieldHit", self, "_on_Shield_hit")
	EventBus.connect("shieldVisibility", self, "_set_shield_glow_visibility")
	$Sprite.material = $Sprite.material.duplicate()
	$Smoke_Trail.material = $Smoke_Trail.material.duplicate()
	_hide_Shield()
	shield_collision.disabled = true
	hurtbox_collision.disabled = false


func _get_local_input() -> Dictionary:
	var input_action
	var input_action_2 

	if Input.is_action_pressed("move_right"):
		input_action = "right"
		if Input.is_action_just_pressed("attack"):
			input_action_2 = "side_attack"
		if Input.is_action_just_pressed("jump"):
			input_action_2 = "run_jump"
		if Input.is_action_pressed("shield"):
			input_action_2 = "run_shield_right"
	elif Input.is_action_just_released("move_right"):
		input_action = "right_released"
	elif Input.is_action_pressed("move_left"):
		input_action = "left"
		if Input.is_action_just_pressed("attack"):
			input_action_2 = "side_attack"
		if Input.is_action_just_pressed("jump"):
			input_action_2 = "run_jump"
		if Input.is_action_pressed("shield"):
			input_action_2 = "run_shield_left"
	elif Input.is_action_just_released("move_left"):
		input_action = "left_released"
	elif Input.is_action_pressed("up"):
		input_action = "up"
		if Input.is_action_just_pressed("attack"):
			input_action_2 = "up_attack_action"
	elif Input.is_action_pressed("down"):
		input_action = "down"
		if Input.is_action_just_pressed("attack"):
			input_action_2 = "down_attack_action"
	if Input.is_action_just_pressed("jump"):
		input_action = "jump"
	if Input.is_action_just_pressed("attack"):
		input_action = "attack"
	if Input.is_action_just_released("attack"):
		input_action = "attack_released"
	if Input.is_action_pressed("shield"):
		input_action = "shield"
		if Input.is_action_just_pressed("move_left"):
			input_action_2 = "shield_roll_left"
		if Input.is_action_just_pressed("move_right"):
			input_action_2 = "shield_roll_right"

	elif Input.is_action_just_released("shield"):
		input_action = "shield_released"

	var input := {}

	if input_action != null:
		input["input_action"] = input_action
		if input_action_2 != null:
			input["input_action"] = input_action_2

	x_motion = SGFixed.from_float(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"))

	return input


var onFloor 
var x_motion : int
var animation 

var hurtbox_area : SGArea2D

var overlapping_areas : Array

var state_name 

var input_to_state



func _network_process(input: Dictionary) -> void:



	shield_disabled = shield_collision.disabled

	match machine_state:

###################         IDLE         ####################

		States.Idle:
			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			wall_check.update_raycast_collision()
			wall_check_2.update_raycast_collision()
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity


			anim_player.play("Idle")
			if onFloor:
				if wall_check.is_colliding():
					if player_dir == 1:
						if input.get("input_action") == "left":
							fixed_scale_x = -65536
							player_dir = -1
							_run()
							machine_state = States.Run
						elif input.get("input_action") == "right":
							_idle()
							machine_state = States.Idle
					if player_dir == -1:
						if input.get("input_action") == "right":
							fixed_scale_x = 65536
							player_dir = 1
							_run()
							machine_state = States.Run
						elif input.get("input_action") == "left":
							_idle()
							machine_state = States.Idle
				else: 
					if input.get("input_action") == "left":
						_run()
						machine_state = States.Run
					elif input.get("input_action") == "right":
						_run()
						machine_state = States.Run
				if input.get("input_action") == "up_attack_action":
					_up_charge()
					machine_state = States.Up_Charge
				if input.get("input_action") == "side_attack":
					if abs(velocity.x) < 114688:
						_charge()
						machine_state = States.Charge
	#					return
				if input.get("input_action") == "jump":
					_jump()
					machine_state = 3
				elif input.get("input_action") == "attack":
					machine_state = 6
					_neutral_attack_1()
				if input.get("input_action") == "shield" || input.get("input_action") == "run_shield_left" || input.get("input_action") == "run_shield_right":
					_shield()
					machine_state = States.Shield
			elif velocity.y > 196608:
				_fall()
				machine_state = 5



###################         RUN         ###################



		States.Run:

			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			wall_check.update_raycast_collision()
			wall_check_2.update_raycast_collision()
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_flip()
			_Shield_regen()
#			anim_player.play("Run")

			if input.get("input_action") == "left":
				player_dir = -1
				if velocity.x > SGFixed.mul(SGFixed.from_float(-1), SPEED):
					velocity.x +=  8192 * player_dir
					
				if wall_check.is_colliding() or wall_check_2.is_colliding():
						if player_dir == -1:
							_idle()
							machine_state = States.Idle

			if input.get("input_action") == "left_released" and velocity.x < 0:
				_idle()
				machine_state = 1

			elif input.get("input_action") == "right":
				player_dir = 1
				if velocity.x < SGFixed.mul(SGFixed.from_float(1), SPEED):
					velocity.x +=  8192 * player_dir
				if wall_check.is_colliding() or wall_check_2.is_colliding():
					if player_dir == 1:
						_idle()
						machine_state = States.Idle

			if input.get("input_action") == "right_released" and velocity.x > 0:
				_idle()
				machine_state = 1


			elif input.get("input_action") == "side_attack":
				if abs(velocity.x) < 57344:
					_charge()
					machine_state = States.Charge
				else:
					_run_attack()
					machine_state = States.Run_Attack
			elif input.get("input_action") == "run_shield_right":
				_shield()
				machine_state = States.Shield
			elif input.get("input_action") == "run_shield_left":
				_shield()
				machine_state = States.Shield
			elif input.get("input_action") == "run_jump":
				_jump()
				machine_state = States.Jump

			if not onFloor and velocity.y > 0:
				_fall()
				machine_state = States.Fall

	###################              JUMP                ###################
	
		States.Jump:
			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			wall_check.update_raycast_collision()
			wall_check_2.update_raycast_collision()
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			_flip()
			velocity.y += gravity

			if onFloor:
				if input.get("input_action") == "left":
					_run()
					machine_state = States.Run
				elif input.get("input_action") == "right":
					_run()
					machine_state = States.Run
				else:
					_idle()
					machine_state = States.Idle
			else: 
				if wall_check.is_colliding() or wall_check_2.is_colliding():
					if player_dir == 1:
						if input.get("input_action") == "left":
							fixed_scale_x = -65536
							player_dir = -1
						elif input.get("input_action") == "right":
							velocity.x = 0
					if player_dir == -1:
						if input.get("input_action") == "right":
							fixed_scale_x = 65536
							player_dir = 1
						elif input.get("input_action") == "left":
							velocity.x = 0

				if input.get("input_action") == "left" || input.get("input_action") == "run_shield_left" || input.get("input_action") == "shield_roll_left":
					velocity.x = SGFixed.mul(SGFixed.from_float(-1), SPEED)
				elif input.get("input_action") == "right" || input.get("input_action") == "run_shield_right" || input.get("input_action") == "shield_roll_right":
					velocity.x = SGFixed.mul(SGFixed.from_float(1), SPEED)
				elif input.get("input_action") != "left" and input.get("input_action") != "right":
					velocity.x = 0
				if input.get("input_action") == "jump" && _jumps_made < 2 and velocity.x == 0:
					_double_jump()
					machine_state = States.Double_Jump
				elif input.get("input_action") == "run_jump" && _jumps_made <2:
					_double_jump()
					machine_state = States.Double_Jump
				if velocity.y > 0 and not onFloor:
					_fall()
					machine_state = States.Fall
				if input.get("input_action") == "attack":
					_n_air()
					machine_state = States.N_Air
				elif input.get("input_action") == "up_attack_action":
					if _up_airs == 0:
						_up_air()
						machine_state = States.Up_Air
				elif input.get("input_action") == "side_attack":
					if _air_kicks == 0:
						_air_kick()
						machine_state = States.Air_Kick
				elif input.get("input_action") == "down_attack_action":
						_d_air()
						machine_state = States.D_Air



	
	###################           DOUBLE JUMP            ###################
	
		States.Double_Jump:
			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			wall_check.update_raycast_collision()
			wall_check_2.update_raycast_collision()
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_flip()

			if onFloor:
				if input.get("input_action") == "left":
					_run()
					machine_state = States.Run
				elif input.get("input_action") == "right":
					_run()
					machine_state = States.Run
				else:
					_idle()
					machine_state = States.Idle
					
			else: 
				if wall_check.is_colliding() or wall_check_2.is_colliding():
					if player_dir == 1:
						if input.get("input_action") == "left":
							fixed_scale_x = -65536
							player_dir = -1
						elif input.get("input_action") == "right":
							velocity.x = 0
					if player_dir == -1:
						if input.get("input_action") == "right":
							fixed_scale_x = 65536
							player_dir = 1
						elif input.get("input_action") == "left":
							velocity.x = 0
				if input.get("input_action") == "left" || input.get("input_action") == "run_shield_left" || input.get("input_action") == "shield_roll_left":
					velocity.x = SGFixed.mul(SGFixed.from_float(-1), SPEED)
				elif input.get("input_action") == "right" || input.get("input_action") == "run_shield_right" || input.get("input_action") == "shield_roll_right":
					velocity.x = SGFixed.mul(SGFixed.from_float(1), SPEED)
				elif input.get("input_action") != "left" and input.get("input_action") != "right":
						velocity.x = 0
				if input.get("input_action") == "attack":
					_n_air()
					machine_state = States.N_Air
				elif input.get("input_action") == "up_attack_action":
					if _up_airs == 0:
						_up_air()
						machine_state = States.Up_Air
				elif input.get("input_action") == "side_attack":
					if _air_kicks == 0:
						_air_kick()
						machine_state = States.Air_Kick
				elif input.get("input_action") == "down_attack_action":
						_d_air()
						machine_state = States.D_Air
				if velocity.y > 0 and not onFloor:
					_fall()
					machine_state = States.Fall
	
	###################               FALL            ###################
	
		States.Fall:

			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			top_raycast.update_raycast_collision()
			bottom_raycast.update_raycast_collision()
			vertical_raycast.update_raycast_collision()
			wall_check.update_raycast_collision()
			wall_check_2.update_raycast_collision()


			if onFloor:
				if input.get("input_action") == "left":
					_run()
					machine_state = States.Run
				elif input.get("input_action") == "right":
					_run()
					machine_state = States.Run
				else:
					_idle()
					machine_state = States.Idle

			else: 
				if input.get("input_action") == null:
					velocity.x = 0
				if wall_check.is_colliding() or wall_check_2.is_colliding():
					if player_dir == 1:
						if input.get("input_action") == "left":
							fixed_scale_x = -65536
							player_dir = -1
						elif input.get("input_action") == "right":
							velocity.x = 0
					if player_dir == -1:
						if input.get("input_action") == "right":
							fixed_scale_x = 65536
							player_dir = 1
						elif input.get("input_action") == "left":
							velocity.x = 0
				if bottom_raycast.is_colliding() && top_raycast.is_colliding() && vertical_raycast.is_colliding():
					_ledge_grab()
					machine_state = States.Ledge_Grab
				else: 
					if input.get("input_action") == "left" || input.get("input_action") == "run_shield_left" || input.get("input_action") == "shield_roll_left":
						_flip()
						velocity.x = SGFixed.mul(SGFixed.from_float(-1), SPEED)
					elif input.get("input_action") == "right" || input.get("input_action") == "run_shield_right" || input.get("input_action") == "shield_roll_right":
						_flip()
						velocity.x = SGFixed.mul(SGFixed.from_float(1), SPEED)
					if input.get("input_action") == "jump" && _jumps_made == 0:
						_jump()
						machine_state = States.Jump
					elif input.get("input_action") == "jump" && _jumps_made == 1 and velocity.x == 0:
						_double_jump()
						machine_state = States.Double_Jump
					elif input.get("input_action") == "run_jump" && _jumps_made == 1:
						_double_jump()
						machine_state = States.Double_Jump
					if input.get("input_action") == "attack":
						_n_air()
						machine_state = States.N_Air
					elif input.get("input_action") == "up_attack_action":
						if _up_airs == 0:
							_up_air()
							machine_state = States.Up_Air
					elif input.get("input_action") == "side_attack":
						if _air_kicks == 0:
							_air_kick()
							machine_state = States.Air_Kick
					elif input.get("input_action") == "down_attack_action":
							_d_air()
							machine_state = States.D_Air



	###################         Neutral Attack 1         ###################
	
		States.Neutral_Attack:
			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			if input.get("input_action") == "attack":
				comboing = true


	###################         Neutral Attack 2         ###################

		States.Neutral_Attack_2:
			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			if input.get("input_action") == "attack":
				comboing = true



	###################         Neutral Attack 3         ###################
	
		States.Neutral_Attack_3:
			_Shield_regen()
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			if input.get("input_action") == "attack":
				comboing = true


	###################            Run Attack            ###################


		States.Run_Attack:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()


			if velocity.x > 0:
				if player_dir > 0:
					if fixed_position_x < fixed_position_x + 40960:
						velocity.x -=  4096 * player_dir
				else: 
					velocity.x = 0
			elif velocity.x < 0:
				if player_dir < 0:
					if fixed_position_x > fixed_position_x - 40960:
						velocity.x -=  4096 * player_dir
				else: 
					velocity.x = 0
	

	###################            Neutral Air           ###################

		States.N_Air:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()

			if input.get("input_action") == "left":
				velocity.x = SGFixed.mul(SGFixed.from_float(-1), SPEED)
			elif input.get("input_action") == "right":
				velocity.x = SGFixed.mul(SGFixed.from_float(1), SPEED)
			if onFloor:
#				_zero_hitbox_values()
				_clear_hitbox_array()
				_idle()
				machine_state = States.Idle



	###################              Up Air              ###################

		States.Up_Air:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()

			if onFloor:
#				_zero_vert_values()
				_clear_up_hitbox_array()
				_idle()
				machine_state = States.Idle



	###################             Air Kick             ###################

		States.Air_Kick:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
#			velocity.y = 0
			_Shield_regen()
			if input.get("input_action") == "left":
				velocity.x = SGFixed.mul(SGFixed.from_float(-1), 100000)
			elif input.get("input_action") == "right":
				velocity.x = SGFixed.mul(SGFixed.from_float(1), 100000)
			if onFloor:
#				_zero_hitbox_values()
				_clear_hitbox_array()
				_idle()
				machine_state = States.Idle



	###################             Down Air             ###################

		States.D_Air:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			_flip()
			_Shield_regen()
			if input.get("input_action") == "left":
				velocity.x = SGFixed.mul(SGFixed.from_float(-1), SPEED)
			elif input.get("input_action") == "right":
				velocity.x = SGFixed.mul(SGFixed.from_float(1), SPEED)
			if velocity.y >= 100000:
				up_hitbox_collision.disabled = false
				velocity.y += gravity * 3
				if onFloor:
					up_hitbox_collision.disabled = true
#					_zero_vert_values()
					_clear_up_hitbox_array()
					_d_air_land()
					machine_state = States.D_Air_Land
					
			else: 
				up_hitbox_collision.disabled = true
				velocity.y += gravity
				if onFloor:
					up_hitbox_collision.disabled = true
#					_zero_vert_values()
					_clear_up_hitbox_array()
					_d_air_land()
					machine_state = States.D_Air_Land



	###################           Down Air Land          ###################

		States.D_Air_Land:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()
			velocity.x = 0



	###################             Charge               ###################


		States.Charge:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.x = 0
			velocity.y += gravity
			_Shield_regen()
			if input.get("input_action") == "attack_released":
				time_difference = networkTimer.wait_ticks - networkTimer.ticks_left
				damage_mul = (1 + SGFixed.to_float(SGFixed.div(time_difference,networkTimer.wait_ticks)))
				networkTimer.stop()
				_charge_attack(round(damage_mul * 4), -round(damage_mul * 3), (round(10 * damage_mul)), player_dir)
				machine_state = States.Charge_Attack
				


	###################            Up Charge             ###################
	
		States.Up_Charge:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.x = 0
			velocity.y += gravity
			_Shield_regen()
			if input.get("input_action") == "attack_released":
				time_difference = networkTimer.wait_ticks - networkTimer.ticks_left
				damage_mul = (1 + SGFixed.to_float(SGFixed.div(time_difference,networkTimer.wait_ticks)))
				networkTimer.stop()
				_up_charge_attack(0, -round(damage_mul * 4), round(damage_mul * 10), player_dir)
				machine_state = States.Up_Charge_Attack
				


	###################           Charge Attack             ###################
	
		States.Charge_Attack:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			velocity.x = 0
			_Shield_regen()
	
	
	
		###################       Up Charge Attack          ###################
	
		States.Up_Charge_Attack:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			velocity.x = 0
			_Shield_regen()
	
	



	###################             Small Hit             ###################
	
		States.Small_Hit:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()


			if velocity.x < 0:
					velocity.x = velocity.x + 8

			elif velocity.x > 0:
					velocity.x = velocity.x - 8




	###################             Big Hit             ###################
	
	
		States.Big_Hit:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()


			if onFloor:
					EventBus.emit_signal("smoke", "disabled", "player_1")
					_down()
					machine_state = States.Down
			elif velocity.y > (196608 * 2):
					_fall()
					EventBus.emit_signal("smoke", "disabled", "player_1")
					machine_state = States.Fall 



	###################             Vert Hit             ###################


		States.Vert_Hit:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			_Shield_regen()


			if onFloor:
				if (velocity.y <= 196608):
					_down()
					machine_state = States.Down 
				else:
					_idle()
					EventBus.emit_signal("smoke", "disabled", "player_1")
					machine_state = States.Idle 




	###################            Ledge Grab            ###################

		States.Ledge_Grab:
			shield_collision.disabled = true
			hurtbox_collision.disabled = true
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
#			velocity.x = 0
#			velocity.y = 0
#			_get_ledge()
			_Shield_regen()
			if player_dir > 0:
	#			if fixed_position_x < ledge_climb_distance:
				velocity.x = SPEED * 2
				velocity.y = 0

	#			else:
	#				velocity.x = 0
			elif player_dir < 0:
	#			if fixed_position_x > ledge_roll_distance:
				velocity.x =  -SPEED * 2
				velocity.y = 0

			if input.get("input_action") == "left":
				if player_dir < 0:
					if bottom_raycast.is_colliding():
						_ledge_roll()
						machine_state = States.Ledge_Roll
				elif player_dir > 0:
					fixed_scale_x = -65536
					player_dir = -1
					_fall()
					machine_state = States.Fall
			if input.get("input_action") == "right":
				if player_dir > 0:
					if bottom_raycast.is_colliding():
						_ledge_roll()
						machine_state = States.Ledge_Roll
				elif player_dir < 0:
					fixed_scale_x = 65536
					player_dir = 1
					_fall()
					machine_state = States.Fall
			if input.get("input_action") == "up":
				_ledge_climb()
				machine_state = States.Ledge_Climb




	###################            Ledge Climb            ###################

		States.Ledge_Climb:
			shield_collision.disabled = true
			hurtbox_collision.disabled = true
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			_Shield_regen()
			

			if (fixed_position_y) <= collision_point_var:

					if player_dir > 0:
						velocity.x = SPEED * 2
						velocity.y = 0 


					elif player_dir < 0:
						velocity.x =  -SPEED * 2
						velocity.y = 0


			else: 
				velocity.y =  -SPEED * 2
				velocity.x = 0



	###################             Ledge Roll            ###################


		States.Ledge_Roll:
			shield_collision.disabled = true
			hurtbox_collision.disabled = true
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			_Shield_regen()



			if (fixed_position_y) <= collision_point_var:

					if player_dir > 0:
						velocity.x = SPEED * 2
						velocity.y = 0 


					elif player_dir < 0:
						velocity.x =  -SPEED * 2
						velocity.y = 0


			else: 
				velocity.y =  -SPEED * 2
				velocity.x = 0


	###################               Shield              ###################


		States.Shield:
			shield_collision.disabled = false
			hurtbox_collision.disabled = true
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity
			
			
			if onFloor:
				if shield_health > 0:
					shield_health -= 1
					_update_shield_opacity()
				else: 
					_shield_broken()
					machine_state = States.Shield_Broken


				if player_dir == 1:
					if input.get("input_action") == "shield_roll_right":# || input.get("input_action") == "shield_run_right":
						_shield_roll("Right")
						machine_state = States.Shield_Roll
					elif input.get("input_action") == "shield_roll_left":# || input.get("input_action") == "shield_run_left":
						_shield_roll("Left")
						machine_state = States.Shield_Roll

				elif player_dir == -1:
					if input.get("input_action") == "shield_roll_left":# || input.get("input_action") == "shield_run_left":
						_shield_roll("Left")
						machine_state = States.Shield_Roll
					elif input.get("input_action") == "shield_roll_right":# || input.get("input_action") == "shield_run_right":
						_shield_roll("Right")
						machine_state = States.Shield_Roll

				if input.get("input_action") == "shield_released":
					_idle()
					machine_state = States.Idle

				if input.get("input_action") != "shield" and input.get("input_action") != "shield_roll_right" and input.get("input_action")!= "shield_roll_left":
					_idle()
					machine_state = States.Idle
			
			else:
				_fall()
				machine_state = States.Fall

	###################             Shield Roll           ###################


		States.Shield_Roll:

			shield_collision.disabled = true
			hurtbox_collision.disabled = true
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			velocity.y += gravity


			if player_dir == 1:
				if roll_dir == "Right":
					velocity.x = SPEED * 3
				elif roll_dir == "Left":
					velocity.x = -SPEED * 3

			elif player_dir == -1:
				if roll_dir == "Right":
					velocity.x = SPEED * 3
				elif roll_dir == "Left":
					velocity.x = -SPEED * 3


	###################            Shield Broken          ###################

		States.Shield_Broken:
			shield_collision.disabled = true
			hurtbox_collision.disabled = false
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
#			shield_collision.disabled = true
			velocity.y += gravity
			velocity.x = 0


	###################                Down               ###################

		States.Down:
			shield_collision.disabled = true
			hurtbox_collision.disabled = true
			velocity = move_and_slide(velocity, up_direction)
			onFloor = is_on_floor()
			_Shield_regen()
			velocity.y += gravity
			if onFloor:
#				if velocity.x != 0:
#					if velocity.x > 0:
#						velocity.x -= 10240
#						if velocity.x < 0:
#							velocity.x = 0
#					if velocity.x < 0:
#						velocity.x += 10240
#						if velocity.x > 0:
							velocity.x = 0
			else: 
				_fall()
				machine_state = States.Fall



	
	
	
#	if self.name == "ClientPlayer":
#		print (onFloor)
#		print (input.get("input_action"))
#		print (shield_health)
#		print (shield_disabled)




func _on_NetworkAnimationPlayer_animation_finished(anim_name):

	if anim_name == "Attack":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		if comboing == true:
			_neutral_attack_2()
			machine_state = States.Neutral_Attack_2
		elif comboing == false:
			_idle()
			machine_state = States.Idle
	elif anim_name == "NeutralAttack2":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		if comboing == true:
			_neutral_attack_3()
			machine_state = States.Neutral_Attack_3
		elif comboing == false:
			_idle()
			machine_state = States.Idle
	elif anim_name == "neutral_attack_3":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		_idle()
		machine_state = States.Idle
	elif anim_name == "Small_Hit":
		if onFloor:
			_idle()
			machine_state = States.Idle
		else:
			_fall()
			machine_state = States.Fall
	elif anim_name == "charge":
		networkTimer.stop()
		_charge_attack(6, -4, 20, player_dir)
		machine_state = States.Charge_Attack
	elif anim_name == "charge_attack":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		machine_state = States.Idle
	elif anim_name == "Up_Charge":
		networkTimer.stop()
		_up_charge_attack(0, -8, 20, player_dir)
		machine_state = States.Up_Charge_Attack
	elif anim_name == "Up_Charge_Attack":
#		_zero_vert_values()
		_clear_up_hitbox_array()
		machine_state = States.Idle
	elif anim_name == "Vertical_Hit":
		if not onFloor:
			EventBus.emit_signal("smoke", "disabled", "player_1")
			_fall()
			machine_state = States.Fall

	elif anim_name == "Run_Attack":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		_idle()
		machine_state = States.Idle 
	elif anim_name == "Ledge_Roll":
		_idle()
		machine_state = States.Idle 
	elif anim_name == "Ledge_Climb":
		_idle()
		machine_state = States.Idle 
	elif anim_name == "N_Air":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		if not onFloor:
			_fall()
			machine_state = States.Fall
	elif anim_name == "Up_Air":
#		_zero_vert_values()
		_clear_up_hitbox_array()
		if not onFloor:
			_fall()
			machine_state = States.Fall
	elif anim_name == "Air_Kick":
#		_zero_hitbox_values()
		_clear_hitbox_array()
		if not onFloor:
			_fall()
			machine_state = States.Fall
	elif anim_name == "Shield_Roll":
		_shield()
		machine_state = States.Shield
	elif anim_name == "Shield_Roll_Backwards":
		_shield()
		machine_state = States.Shield
	elif anim_name == "d_air_land":
#		_zero_splash_values()
		_clear_d_air_splash_array()
		_idle()
		machine_state = States.Idle
	elif anim_name == "Shield_Broken":
		_idle()
#		shield_collision.disabled = true
#		hurtbox_collision.disabled = false
		machine_state = States.Idle
	elif anim_name == "Down":
		_idle()
#		shield_collision.disabled = true
#		hurtbox_collision.disabled = false
		machine_state = States.Idle
#	sync_to_physics_engine()



func _save_state() -> Dictionary:
	return {
		state_machine = machine_state,
		fixed_position_x = fixed_position.x,
		fixed_position_y = fixed_position.y,
		Is_on_floor = onFloor,
		fixed_scale_x = fixed_scale.x,
		velocity_x = velocity.x,
		velocity_y = velocity.y,
		hit_velocity_x = hit_velocity.x,
		hit_velocity_y = hit_velocity.y,
		air_kicks = _air_kicks,
		up_airs = _up_airs,
		comboing = comboing,
		jumps_made = _jumps_made, 
		Time_difference = time_difference,
		Damage_mul = damage_mul,
		player_Dir = player_dir,
		damage_mult = damage_multiplier,
		Health = health,
		shield_Health = shield_health,
		networktimer_wait_ticks = networkTimer.wait_ticks, 
		networktimer_ticks_left = networkTimer.ticks_left,
		backwards_roll = backwards,
		roll_Dir = roll_dir,
		Recieve_damage_dealt = recieve_damage_dealt,
		Opponent_dir = opponent_dir,
		Opponent_mul = opponent_multiplier,
		Recieve_dam_dealt = recieve_damage_dealt,
		Recieve_impulse_x = recieve_impulse_x,
		Recieve_impulse_y = recieve_impulse_y,
		Player_name = player_name,
		Stock = stock, 
		Hurtbox_collision = hurtbox_collision.disabled,
		Shield_collision = shield_disabled,
		vert_hitbox = up_hitbox_collision.disabled
	}


func _load_state(state: Dictionary) -> void:
	
	machine_state = state['state_machine']
	fixed_position.x = state["fixed_position_x"]
	fixed_position.y = state["fixed_position_y"]
	onFloor = state['Is_on_floor']
	fixed_scale.x = state["fixed_scale_x"]
	velocity.x = state['velocity_x']
	velocity.y = state['velocity_y']
	_air_kicks = state['air_kicks']
	_up_airs = state['up_airs']
	comboing = state['comboing']
	_jumps_made = state['jumps_made']
	player_dir = state['player_Dir']
	damage_multiplier = state['damage_mult']
	health = state['Health']
	shield_health = state['shield_Health']
	time_difference = state['Time_difference']
	damage_mul = state['Damage_mul']
	networkTimer.wait_ticks = state['networktimer_wait_ticks']
	networkTimer.ticks_left = state['networktimer_ticks_left']
	backwards = state['backwards_roll']
	roll_dir = state['roll_Dir']
	opponent_dir = state['Opponent_dir']
	opponent_multiplier = state['Opponent_mul']
	recieve_damage_dealt = state['Recieve_dam_dealt']
	recieve_impulse_x = state['Recieve_impulse_x']
	recieve_impulse_y = state['Recieve_impulse_y']
	hit_velocity.x = state['hit_velocity_x']
	hit_velocity.y = state['hit_velocity_y']
	player_name = state['Player_name']
	stock = state['Stock']
	hurtbox_collision.disabled = state['Hurtbox_collision']
	shield_disabled = state['Shield_collision']
	up_hitbox_collision.disabled = state['vert_hitbox']
	sync_to_physics_engine()


func _bounce():
	velocity.y = velocity.y * -1

func _idle():
	EventBus.emit_signal("smoke", "disabled", "player_1")
	anim_player.play("Idle")
#	shield_collision.disabled == true
#	$HurtBox/Hurtbox_Collision.disabled == false
	velocity.x = 0
	_air_kicks = 0
	_up_airs = 0
	comboing = false
	climb_raycast_enabled()
	_jumps_made = 0
	_hide_Shield()
	recieve_damage_dealt = 0
	opponent_dir = 0
	opponent_multiplier = 0
	sync_to_physics_engine()

func _run():
	velocity.x = 8196 * player_dir
	anim_player.play("Run")
	_air_kicks = 0
	_up_airs = 0
	_jumps_made = 0
	_hide_Shield()
#	EventBus.connect("invincibility_over", self, "_set_collision")
#	if player.invincible == false:
#		EventBus.emit_signal("hurtBoxShieldCollision", "enabled", "disabled")



func _jump():
	anim_player.play("Jump")
	_jumps_made += 1
	velocity.y = -JUMP
#	sync_to_physics_engine()


func _double_jump():
	anim_player.play("Double_Jump")
	_jumps_made += 1
	velocity.y = -DOUBLE_JUMP
#	sync_to_physics_engine()

func _fall():
	anim_player.play("Fall")
	EventBus.emit_signal("smoke", "disabled", "player_1")
	_hide_Shield()


func _neutral_attack_1():
	comboing = false
	velocity.x = 0
	anim_player.play("Attack")
	hitbox._set_values(0, 0, 3, player_dir)
#	sync_to_physics_engine()

func _neutral_attack_2():
	comboing = false
	velocity.x = 0
	anim_player.play("NeutralAttack2")
	hitbox._set_values(0, 0, 3, player_dir)
#	sync_to_physics_engine()


func _neutral_attack_3():
	comboing = false
	velocity.x = 0
	anim_player.play("neutral_attack_3")
	hitbox._set_values(1, -1, 3, player_dir)
#	sync_to_physics_engine()


func _run_attack():
	anim_player.play("Run_Attack")
	hitbox._set_values(2, -1, 5, player_dir)


func _n_air():
	anim_player.play("N_Air")
	hitbox._set_values(2, -2, 5, player_dir)


func _up_air():
	_up_airs += 1
	anim_player.play("Up_Air")
	$Sword_Hitbox_up._set_values(0, -4, 5, player_dir, "up_air")


func _air_kick():
	_air_kicks += 1
	anim_player.play("Air_Kick")
	hitbox._set_values(4, -1, 7, player_dir)


func _d_air():
	anim_player.play("d_air")
	$Sword_Hitbox_up._set_values(0, 6, 8, player_dir, "d_air")


func _d_air_land():
	anim_player.play("d_air_land")
	#camera_shake()
	d_air_splash._set_values(0, -4, 7, player_dir)


func _charge():
	anim_player.play("charge")
	networkTimer.wait_ticks = 120
	networkTimer.start()


func _up_charge():
	anim_player.play("Up_Charge")
	networkTimer.wait_ticks = 120
	networkTimer.start()


func _charge_attack(_impulse_x, _impulse_y, _damage, _player_dir):

	_resetColor()
	anim_player.play("charge_attack")
	hitbox._set_values(_impulse_x, _impulse_y, _damage, _player_dir)


func _up_charge_attack(_impulse_x, _impulse_y, _damage, _player_dir):

	_resetColor()
	anim_player.play("Up_Charge_Attack")
	$Sword_Hitbox_up._set_values(_impulse_x, _impulse_y, _damage, _player_dir, "up_charge")


func _shield():
	velocity.x = 0
	anim_player.play("Shield")
	_show_Shield()



func _shield_roll(direction: String):
	_hide_Shield()
	
	if player_dir == 1:
		if direction == "Right":
			velocity.x = (SPEED * 3)
			backwards = false
			roll_dir = "Right"
		elif direction == "Left":
			velocity.x = -(SPEED * 3)
			backwards = true
			roll_dir = "Left"
	elif player_dir == -1:
		if direction == "Right":
			velocity.x = (SPEED * 3)
			backwards = true
			roll_dir = "Right"
		elif direction == "Left":
			velocity.x = -(SPEED * 3)
			backwards = false
			roll_dir = "Left"

	if backwards == false:
		anim_player.play("Shield_Roll")
	elif backwards == true:
		anim_player.play("Shield_Roll_Backwards")



func _shield_broken():
#	$HurtBox/Hurtbox_Collision.disabled == false
#	shield_collision.disabled == true
	_hide_Shield()
	anim_player.play("Shield_Broken")


func _down():
	anim_player.play("Down")
#	shield_collision.disabled == true
#	$HurtBox/Hurtbox_Collision.disabled == true

func _clear_hitbox_array():
	hitbox.hit_targets.clear()

#func _zero_hitbox_values():
#	hitbox._set_values(0,0,0, player_dir)
#
#func _zero_vert_values():
#	$Sword_Hitbox_up._set_values(0,0,0, player_dir, "")
#
#func _zero_splash_values():
#	d_air_splash._set_values(0,0,0, player_dir)

#func _clear_vert_hitbox_array():
#	vert_hitbox.hit_targets.clear()

func _clear_up_hitbox_array():
	$Sword_Hitbox_up.hit_targets.clear()

func _clear_d_air_splash_array():
	d_air_splash.hit_targets.clear()


func climb_raycast_enabled():
	vertical_raycast.collide_with_bodies = true
	top_raycast.collide_with_bodies = true
	bottom_raycast.collide_with_bodies = true


func climb_raycast_disabled():
	vertical_raycast.collide_with_bodies = false
	top_raycast.collide_with_bodies = false
	bottom_raycast.collide_with_bodies = false





#func get_fall_speed_near_ground():
#	if $ground_raycast.is_colliding():
#		velocity_near_ground = _velocity.y





func _get_ledge(): 
 
	collision_point.x =  (vertical_raycast.get_collision_point().x)
	collision_point.y =  (vertical_raycast.get_collision_point().y)
	collision_point_var = collision_point.y - 3000000
	ledge_roll_distance = fixed_position_x + (7000064*player_dir)
	ledge_climb_distance = fixed_position_x + (2000896*player_dir)


func _ledge_grab():
	_air_kicks = 0
	_up_airs = 0
	anim_player.play("Ledge_Grab")
	_get_ledge()
	_jumps_made = 0


func _ledge_climb():
#	climb_raycast_disabled()
	anim_player.play("Ledge_Climb")


func _ledge_roll():
#	climb_raycast_disabled()
	anim_player.play("Ledge_Roll")



func _flip():

	#Flip animations based on player_dir
#	if Input.is_action_pressed("move_left"):
	if velocity.x < 0:
		fixed_scale_x = -65536
		player_dir = -1

	elif velocity.x > 0: 
		fixed_scale_x = 65536
		player_dir = 1

	sync_to_physics_engine()
	

func _flip_when_hurt():

	fixed_scale_x = -fixed_scale_x 
	player_dir = -player_dir 
	sync_to_physics_engine()



onready var hit_velocity := SGFixed.vector2(0,0)


func _on_HurtBox_hit(hitbox_parent, _player_dir: int, damage : int, _impulse_x : int, _impulse_y: int):


	if hitbox_parent != self.name:
		recieve_damage_dealt = damage 
		opponent_dir = _player_dir


		if health == 0:
			opponent_multiplier =  (4096 + SGFixed.div(health,50))
		else:
			opponent_multiplier =  SGFixed.div(health,50)

		recieve_impulse_x = SGFixed.mul(SGFixed.from_float(_impulse_x), opponent_multiplier)
		recieve_impulse_y = SGFixed.mul(SGFixed.from_float(_impulse_y), opponent_multiplier)
		player_name = str(self.name)
		hit_velocity.x = SGFixed.mul(SGFixed.from_float(opponent_dir), recieve_impulse_x)
		hit_velocity.y = (recieve_impulse_y)
		health += recieve_damage_dealt
		_updateHealth()
		_resetColor()


		if abs(hit_velocity.y) > abs(hit_velocity.x):
			if abs(hit_velocity.y) >= 196608:
				velocity.x = 0
				velocity.y = hit_velocity.y
				machine_state = States.Vert_Hit
				EventBus.emit_signal("smoke", "enabled", "player_1")
				anim_player.play("Vertical_Hit")
			else:
				velocity.x = 0
				velocity.y = hit_velocity.y
				machine_state = States.Small_Hit
				anim_player.play("Small_Hit")
		elif abs(hit_velocity.y) <= abs(hit_velocity.x):
			if abs(hit_velocity.x) > 196608 * 2:
					if player_dir == opponent_dir:
						_flip_when_hurt()
					velocity.x = hit_velocity.x
					velocity.y = hit_velocity.y
					machine_state = States.Big_Hit
					EventBus.emit_signal("smoke", "enabled", "player_1")
					anim_player.play("Big_Hit")
			elif abs(hit_velocity.x) <= 196608 * 2:
					if player_dir == opponent_dir:
						_flip_when_hurt()
					velocity.x = hit_velocity.x
					velocity.y = hit_velocity.y
					machine_state = States.Small_Hit
					anim_player.play("Small_Hit")



func _vert_hit():
	EventBus.emit_signal("smoke", "enabled", "player_1")
	velocity.x = 0
	velocity.y = hit_velocity.y
	anim_player.play("Vertical_Hit")


func _small_hit():
	velocity.x = hit_velocity.x
	velocity.y = hit_velocity.y
	anim_player.play("Small_Hit")


func _big_hit():
	EventBus.emit_signal("smoke", "enabled", "player_1")
	velocity.x = hit_velocity.x
	velocity.y = hit_velocity.y
	anim_player.play("Big_Hit")




func _updateHealth():
	EventBus.emit_signal("healthUpdate", health, player_name)
	

func _show_Shield():
	$Sprite/Shield_Sprite.visible = true

func _hide_Shield():
	$Sprite/Shield_Sprite.visible = false



func _update_shield_opacity():
		$Sprite/Shield_Sprite.modulate.a = ((max_shield_health - shield_health)/max_shield_health)


func _on_Shield_hit(hitbox_parent, _player_dir: int, damage):
#	EventBus.emit_signal("cameraShake", enemy_damage, health, enemy_impulse_x, enemy_impulse_y)


	if hitbox_parent != str(self):
		recieve_damage_dealt = damage 
		if shield_health > 0:
			shield_health -= clamp (damage, 0, 300)
			_update_shield_opacity()
		player_name = str(self.name)

 


func _Shield_regen():
	if shield_health < 300:
		shield_health += 1


func _on_Shield_deplete(delta):
	if shield_health > 0:
		_update_shield_opacity()
		shield_health -= 1



# Spawn System 

var new_position:= Vector2(-200,-400)

func _spawn():
	_updateStock()
	_spawnPosition()
	_hide_Shield()
	velocity.y = 0
	health = 0
	shield_health = 300
	_updateHealth()
	sync_to_physics_engine()
	_idle()
	machine_state = States.Idle


func _updateStock():
	stock = clamp(stock - 1, 0, 3)
	EventBus.emit_signal("dead", self.name, stock)


func _spawnPosition():
#	EventBus.emit_signal("K_O_CameraShake")
#	yield(get_tree().create_timer(1.5), "timeout")
	fixed_position_x = 33751040
	fixed_position_y = 2818048
	sync_to_physics_engine()

#func _spawn(invincibilityTime):
#	invincibilityTimer.set_wait_time(invincibilityTime)
#	_invincible(invincibilityTime)
#	$StateMachine.transition_to("Idle")
#	health = 0
#	shield_health = 350
#	_updateHealth()



# Invincibility 


onready var invincibilityTimer = $InvincibilityTimer
var invincible : bool = false


#func _invincible(invincibilityTime):
#	invincible = true
#	invincibilityTimer.set_one_shot(false)
#	_hurtbox_and_shield_collision("disabled", "disabled")
#	invincibilityTimer.start()
#	animPlayerInvincibility.play("Invincible")



#func _on_InvincibilityTimer_timeout():
#	invincible = false
#	invincibilityTimer.stop()
#	animPlayerInvincibility.stop(true)
#	EventBus.emit_signal("invincibility_over")
#	_resetColor()

func _resetColor():
	$Sprite.modulate.a = 1
	$Sprite.get_material().set_shader_param("flash_modifier", 0)


#hurtbox and shield collisions

#func _hurtbox_and_shield_collision(hurtbox, shield):
#	if hurtbox == "enabled":
#		hurtbox_collision.set_deferred("disabled", false)
#	if hurtbox == "disabled":
#		hurtbox_collision.set_deferred("disabled", true)
#	if shield == "enabled":
#		shield_collision.set_deferred("disabled", false)
#	if shield == "disabled":
#		shield_collision.set_deferred("disabled", true)

# hitbox collisions

#func _vertical_Hitbox_Collision(hitbox):
#	if hitbox == "enabled":
#		vertical_hitbox_collision.set_deferred("disabled", false)
#	if hitbox == "disabled":
#		vertical_hitbox_collision.set_deferred("disabled", true)

#func _d_air_hit():
#	_velocity.y = -400



# smoke 

onready var smoke = $Smoke_Trail

func _smokeEnabled(toggle, player):
#	yield(get_tree().create_timer(.1), "timeout")
#	if player == "player_1":
	if machine_state == States.Big_Hit:
		if toggle == "enabled":
			smoke.set_emitting(true)
		elif toggle == "disabled":
			smoke.set_emitting(false) 




func _on_NetworkTimer_timeout():
	pass # Replace with function body.
