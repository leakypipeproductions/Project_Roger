extends SGArea2D


var colliders 
var hitbox_parent 
var collision_shapes

var hit_targets : Array = []

# Called when the node enters the scene tree for the first time.
func _ready():
	colliders = null
	hitbox_parent = null



func _network_process(input: Dictionary) -> void:
	sync_to_physics_engine()
	
	collision_shapes = get_overlapping_bodies()
	colliders = get_overlapping_areas()

	if collision_shapes.size() > 0:
		for bodies in collision_shapes:
			if bodies.is_in_group("player"):
				if bodies.has_method("_spawn"):
					bodies._spawn()

	else: 
		hitbox_parent = null
		colliders = null



func _save_state() -> Dictionary:
		return {
		}


func _load_state(state: Dictionary) -> void:
	pass
