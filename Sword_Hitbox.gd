extends SGArea2D

var player = Player

var player_dir : int = 1
var player_damage : int
var player_impulse_x 
var player_impulse_y
var impulse_vector := SGFixed.vector2(0,0)
var player_damage_mul 
onready var sword_collision = $Sword_Hitbox_Collision

var colliders : Array = []
var hitbox_parent 
var collision_shapes
var collider_var 

var hit_targets : Array = []

# Called when the node enters the scene tree for the first time.
func _ready():
	player = owner as Player
	colliders = []
	hitbox_parent = null
	collider_var = null




func _set_values(_impulse_x, _impulse_y, _damage, _player_dir):
	impulse_vector.x = _impulse_x
	impulse_vector.y = _impulse_y
	player_damage = _damage
	player_dir = _player_dir



func _network_process(input: Dictionary) -> void:
	sync_to_physics_engine()



	if get_overlapping_areas().size() > 0:
		for collider in get_overlapping_areas():
			if collider.get_parent().name != get_parent().name:
				collider_var = collider.name
				if collider_var == "HurtBox":
					if !hit_targets.has(collider_var):
						hit_targets.push_back(collider_var)
						hitbox_parent = get_parent().name
						collider.get_parent()._on_HurtBox_hit(hitbox_parent, player_dir, player_damage, impulse_vector.x, impulse_vector.y)


				elif collider_var == "Shield":
					if !hit_targets.has(collider_var):
						hit_targets.push_back(collider_var)
						hitbox_parent = get_parent().name
						collider.get_parent()._on_Shield_hit(hitbox_parent, player_dir, player_damage)





func _save_state() -> Dictionary:
	return {
		fixed_position_x = fixed_position.x,
		fixed_position_y = fixed_position.y,
		fixed_scale_x = fixed_scale.x,
		fixed_scale_y = fixed_scale.y,
		sword_Collision = sword_collision.disabled,
		Hitbox_parent = hitbox_parent,
		Player_dir = player_dir,
		Player_damage = player_damage, 
		Impulse_vector_x = impulse_vector.x,
		Impulse_vector_y = impulse_vector.y,
		Collider_var = collider_var,
		Hit_targets = hit_targets.duplicate(),
		}


func _load_state(state: Dictionary) -> void:
	fixed_position.x = state.fixed_position_x
	fixed_position.y = state.fixed_position_y
	fixed_scale.x = state['fixed_scale_x']
	fixed_scale.y = state['fixed_scale_y']
	sword_collision.disabled = state['sword_Collision']
	hitbox_parent = state['Hitbox_parent']
	player_dir = state['Player_dir']
	player_damage = state['Player_damage']
	impulse_vector.x = state['Impulse_vector_x']
	impulse_vector.y = state['Impulse_vector_y']
	collider_var = state['Collider_var']
	hit_targets = state['Hit_targets'].duplicate()
	sync_to_physics_engine()


