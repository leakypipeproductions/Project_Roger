extends SGArea2D

var player = Player

var player_dir : int = 0
var player_damage : int
var player_impulse_x 
var player_impulse_y
var impulse_vector := SGFixed.vector2(0,0)
var player_damage_mul 
onready var splash_collision = $D_Air_splash_collision

var colliders : Array = []
var hitbox_parent 
var collision_shapes
var ground_collision_var
var collider_var 

var hit_targets : Array = []

# Called when the node enters the scene tree for the first time.
func _ready():
	player = owner as Player
	colliders = []
	hitbox_parent = null
	collider_var = null

func _set_values(_impulse_x, _impulse_y, _damage, _player_dir):
	impulse_vector.x = _impulse_x
	impulse_vector.y = _impulse_y
	player_damage = _damage
	player_dir = _player_dir


func _network_process(input: Dictionary) -> void:
	sync_to_physics_engine()
	


	if get_overlapping_bodies().size() > 0:
		for collisions in get_overlapping_bodies():
			if collisions.get_collision_layer() == 1: 
				if get_overlapping_areas().size() > 0:
					for collider in get_overlapping_areas():
						if collider.get_parent().name != get_parent().name:
							collider_var = collider.name
							if collider_var == "HurtBox":
								if !hit_targets.has(collider_var):
									hit_targets.push_back(collider_var)
									hitbox_parent = get_parent().name
									collider.get_parent()._on_HurtBox_hit(hitbox_parent, player_dir, player_damage, impulse_vector.x, impulse_vector.y)

							elif collider_var == "Shield":
								if !hit_targets.has(collider_var):
									hit_targets.push_back(collider_var)
									hitbox_parent = get_parent().name
									collider.get_parent()._on_Shield_hit(hitbox_parent, player_dir, player_damage)


#				else: 
#					hitbox_parent = null
#					colliders = []




func _save_state() -> Dictionary:
	return {
		fixed_position_x = fixed_position.x,
		fixed_position_y = fixed_position.y,
		fixed_scale_x = fixed_scale.x,
		fixed_scale_y = fixed_scale.y,
		d_air_splash_collision = $D_Air_splash_collision.disabled,
		player_Dir = player_dir,
		player_Damage = player_damage,
#		player_Impulse_x = player_impulse_x,
#		player_Impulse_y = player_impulse_y,
		Impulse_vector_x = impulse_vector.x,
		Impulse_vector_y = impulse_vector.y,
		player_damage_Mul = player_damage_mul,
#		Colliders = colliders,
		Hitbox_parent = hitbox_parent,
		hit_Targets = hit_targets.duplicate(),
		Collider_var = collider_var,
#		Ground_collision_var = ground_collision_var
	}


func _load_state(state: Dictionary) -> void:
	fixed_position.x = state.fixed_position_x
	fixed_position.y = state.fixed_position_y
	fixed_scale.x = state.fixed_scale_x
	fixed_scale.y = state.fixed_scale_y
	$D_Air_splash_collision.disabled = state['d_air_splash_collision']
	player_dir = state['player_Dir']
	player_damage = state['player_Damage']
#	player_impulse_x = state['player_Impulse_x']
#	player_impulse_y = state['player_Impulse_y']
	impulse_vector.x = state['Impulse_vector_x']
	impulse_vector.y = state['Impulse_vector_y']
	player_damage_mul = state['player_damage_Mul']
	collider_var = state['Collider_var']
#	ground_collision_var = state['Ground_collision_var']
#	colliders = state['Colliders']
	hitbox_parent = state['Hitbox_parent']
	hit_targets = state['hit_Targets'].duplicate()
	sync_to_physics_engine()
